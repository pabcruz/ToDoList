package cl.ubb.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.ToDoDao;
import cl.ubb.model.ToDo;

public class ToDoService {

	@Autowired
	private static ToDoDao toDoDao;
	
	@Autowired
	public ToDoService(ToDoDao toDoDao){
		ToDoService.toDoDao=toDoDao;
	}

	

	public ToDo buscarToDo(long id) {
		// TODO Auto-generated method stub
		ToDo toDo=new ToDo();
		toDo= toDoDao.findOne(id);
		
		return toDo;
	}
	
	public ToDo buscarToDo(String categoria) {
		// TODO Auto-generated method stub
		ToDo toDo=new ToDo();
		toDo= toDoDao.findByCategoria(categoria);
		
		return toDo;
	}
	
	public ToDo buscarToDo(Boolean estado) {
		// TODO Auto-generated method stub
		ToDo toDo=new ToDo();
		toDo= toDoDao.findByEstado(estado);
		
		return toDo;
	}



	public static ArrayList<ToDo> retornarTodos() {
		// TODO Auto-generated method stub
		ArrayList<ToDo> misToDos = new ArrayList<ToDo>();
				
		misToDos = (ArrayList<ToDo>)toDoDao.findAll();
				
		return misToDos;
	}



	public ToDo GuardarToDo(ToDo toDo) {
		// TODO Auto-generated method stub
		return toDoDao.save(toDo);
	}
	
}
