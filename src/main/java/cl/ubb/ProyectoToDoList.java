package cl.ubb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoToDoList {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoToDoList.class, args);
	}
}
