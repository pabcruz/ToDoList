package cl.ubb;


import static org.mockito.Mockito.when;

import java.util.ArrayList;

import static org.mockito.Matchers.anyLong;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


import cl.ubb.dao.ToDoDao;
import cl.ubb.model.ToDo;
import cl.ubb.service.ToDoService;

@RunWith(MockitoJUnitRunner.class)
public class ToDoServicesTest {

	@Mock
	private ToDoDao toDoDao;
	
	@InjectMocks
	private ToDoService toDoService;
	
	
	@Test
	public void obtenerUnToDoPorId(){
		//arrange 
		ToDo toDo=new ToDo();
		ToDo toDoEnBusqueda=new ToDo();
		
		//act 
		when(toDoDao.findOne(anyLong())).thenReturn(toDo);
		toDoEnBusqueda=toDoService.buscarToDo(toDo.getId());
		
		//assert 
		Assert.assertNotNull(toDoEnBusqueda);
		Assert.assertEquals(toDo, toDoEnBusqueda);
	}
	
	@Test
	public void obtenerUnToDoPorcategoria(){
		//arrange
		ToDo toDo=new ToDo();
		ToDo toDoEnBusqueda=new ToDo();
		toDo.setCategoria("Hogar");
		
		//act

		when(toDoDao.findByCategoria("Hogar")).thenReturn(toDo);
		toDoEnBusqueda=toDoService.buscarToDo(toDo.getCategoria());
		
		//assert 
		Assert.assertNotNull(toDoEnBusqueda);
		Assert.assertEquals(toDo, toDoEnBusqueda);
	}
	
	@Test
	public void obtenerUnToDoPorEstado(){
		//arrange
		ToDo toDo=new ToDo();
		ToDo toDoEnBusqueda=new ToDo();
		toDo.setEstado(true);
		
		//act
		when(toDoDao.findByEstado(true)).thenReturn(toDo);
		toDoEnBusqueda=toDoService.buscarToDo(toDo.getEstado());
	
		//assert 
		Assert.assertNotNull(toDoEnBusqueda);
		Assert.assertEquals(toDo, toDoEnBusqueda);
	}
	
	@Test
	public void obtenerTodosLosToDo(){
	
		//arrange
		ArrayList<ToDo> ToDos=new ArrayList<ToDo>();
		ArrayList<ToDo> ListadoDeToDos=new ArrayList<ToDo>();;
		
		//act
		when(toDoDao.findAll()).thenReturn(ToDos);
		ListadoDeToDos=ToDoService.retornarTodos();
		
		//assert
		Assert.assertNotNull(ListadoDeToDos);
		Assert.assertEquals(ToDos, ListadoDeToDos);
		
	}
	
	@Test
	public void guardarUnToDo(){
		//arrange
		ToDo toDo=new ToDo();
		ToDo toDoGuardado= new ToDo();
		
		//act 
		when(toDoDao.save(toDo)).thenReturn(toDo);
		toDoGuardado= toDoService.GuardarToDo(toDo);
		
		//assert 
		Assert.assertNotNull(toDoGuardado);
		Assert.assertEquals(toDo, toDoGuardado);
		
		
	}
}
